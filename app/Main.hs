module Main where

import Data.List
import Data.List.Split
import Data.Char
import Data.Containers.ListUtils

data Sudoku = Sudoku [[Maybe Int]]
              deriving Show

type Block = [Maybe Int]

rows :: Sudoku -> [[Maybe Int]]
rows (Sudoku rs) = rs

allBlankSudoku :: Sudoku
allBlankSudoku = Sudoku (take 9 (repeat(take 9 (repeat Nothing))))

isSudoku :: Sudoku -> Bool
isSudoku s
  | length (rows s) /= 9 = False
  | any (\x -> length x /= 9) (rows s) = False
  | any (\x -> any (\x -> x > Just 9 || x < Just 1) (filter(\x -> x /= Nothing) x)) (rows s) = False
  | otherwise = True

isSolved :: Sudoku -> Bool
isSolved s
  | not (isSudoku s) = False
  | any (\x -> any (\x -> x == Nothing) x) (rows s) = False
  | otherwise = True

printSudoku :: Sudoku -> IO()
printSudoku s =
  putStrLn $ concat $ map(++['\n']) $ map(map(toStr)) (rows s)
  where
    toStr (Just a) = head $ show a
    toStr (Nothing) = '.'


readSudoku :: FilePath -> IO(Sudoku)
readSudoku file = do
  f <- readFile file
  return (Sudoku(chunksOf 9 $ map(toSudokuCell) $ filter(\x -> x /= '\n' && x /= ' ') f))
    where
      toSudokuCell '.' = Nothing
      toSudokuCell a = Just (digitToInt(a))

isOkayBlock :: Block -> Bool
isOkayBlock b =
 (length $ nubOrd bl) == (length bl)
 where
   bl = filter(\x -> x/= Nothing) b

blocks :: Sudoku -> [Block]
blocks s =
  allColumns (rows s) ++ (rows s)
  where
    allColumns s = go'  s []
      where
        go' s r
          | (length $ head s) == 0 =  r
          | otherwise = go' (map tail s) ((map head s) : r)
      
-- isOkay :: Sudoku -> Bool

main :: IO ()
main = do
  let s=[[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
  putStrLn $ show $ concat $ map(chunksOf 2) s
